# Api Supercaro

SpringBoot REST Api

# Install dependencies
* MySQL
* Git
* Java 8

# Using MAKE
Make command reference ([Command reference](./USING.md))

# Execute

Before run Check database connection ([application-dev.yaml](./src/main/resources/application-dev.yaml))

Run specifying spring profile dev:

```sh
$ make run JVM_ARG="-Dspring.profiles.active=dev"
```
You can test API health (check server port config on application-{profile}.yml):

```sh
$ curl http://localhost:8081/api-supercaro/v1/private/health
```

## Database 
- Database Model MR PDF ([DBModelPDF](./SupercaroModel.pdf))
- Database Script ([scriptSQL](./supercaro.sql))

## Available Endpoints
Check on swagger UI or doc!

# Test
Unit Test Execution:

```sh
$ make unit-test
```

## Swagger
JSON file:

```
http://localhost:8081/api-supercaro/v1/doc
```
Swagger UI:

```
http://localhost:8081/api-supercaro/v1/swagger-ui.html
```

## Contact
#### Email:
someemail@gmail.com