package com.supercaro.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Date Helper Class
 */
public class DateHelper {
	private DateHelper() {
		throw new UnsupportedOperationException("Should not be instantiated");
	}

	/**
	 * Method that return java.util.Date Now string
	 * @return String
	 */
	public static String dateNow() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		return format.format(date);
	}

	/**
	 * Method that format a java.util.Date to string
	 * @return String
	 */
	public static Date stringToDate(String dateString) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			 date = format.parse(dateString);
		} catch (ParseException e) {
			//TODO
		}
		return date;
	}
}