package com.supercaro.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static com.google.common.base.Predicates.not;

/**
 * Configuration class that setup Swagger
 * @author supercaro
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    public static final String DEFAULT_PROFILE = "default";
	
    /**
     * Defines basic information about your API (as the path mapping and the API info).
     * @return Docket
     */
    @Bean
    public Docket documentation() {
    	String activeProfile = System.getProperty("spring.profiles.active");
        boolean disableSwaggerProd = "prod".equalsIgnoreCase(activeProfile) ? false : true;
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
                .build()
                .pathMapping("/")
                .useDefaultResponseMessages(false)
                .apiInfo(metadata())
                .enable(disableSwaggerProd);
    }
    
    /**
     * Method for config Swagger
     * @return UiConfiguration
     */
    @Bean
    public UiConfiguration uiConfig() {
        return new UiConfiguration(
                null,
                "list",
                "alpha",
                "schema",
                UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS,
                false,
                true,
                60000L);
    }
	
    /**
     * Generates metadata that represents the general information
     * 	about the API or service being constructed.
     * @return ApiInfo
     */
    protected ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("Api Supercaro")
                .description("Api Supercaro")
                .version("1.0")
                .contact(new Contact("Leonardo RD", "", "leonardord8@gmail.com"))
                .build();
    }
    
    protected final String getProfile(Environment environment) {
        String profile = "default";
        if (environment.getActiveProfiles().length > 0) {
            profile = environment.getActiveProfiles()[0];
        } else if (environment.getDefaultProfiles().length > 0) {
            profile = environment.getDefaultProfiles()[0];
        }
        return profile;
    }
}