package com.supercaro.web.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This adapter makes it easier to implement new filters using the Spring
 * approach of Interceptors. Each new filter should be a subclass of
 * FilterAsInterceptorAdapter, overriding its methods at will.
 */
public abstract class FilterAsInterceptorAdapter implements Filter {
	
    /**
     * Usually you wouldn't have to overwrite this method again, but if for
     * some reason you have to do it, be careful with the methods' calling order.
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        if (preHandle(httpServletRequest, httpServletResponse)) {
            chain.doFilter(httpServletRequest, httpServletResponse);
        }
        afterCompletion(httpServletRequest, httpServletResponse);
    }

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response) {
        //TODO: Logger
        return true;
    }

    /***
     * Override this method in your concrete filter to make your own afterCompletion logic
     */
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response) {
        //TODO: Logger
    }

    protected boolean excludeFromLog(String requestPath, String[] excludedPaths) {
        if (excludedPaths != null){
            for (String  excludePath: excludedPaths) {
                if (requestPath.contains(excludePath)) {
                    return true;
                }
            }
        } else {
           //TODO: Logger
        }
        return false;
    }


    /***
     * Override this method in your concrete filter to make your own destroy logic
     */
    @Override
    public void destroy() {
        //TODO: Logger
    }

    /***
     * Override this method in your concrete filter to make your own init logic
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //TODO: Logger
    }
}