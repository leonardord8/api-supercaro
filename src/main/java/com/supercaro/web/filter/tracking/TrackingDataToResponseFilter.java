package com.supercaro.web.filter.tracking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.supercaro.web.filter.FilterAsInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class that extracts tracking headers from the request and attach
 * them to the response. The tracking headers taken from the request
 * were created when the request was intercepted for the first
 * time.
 *
 * @see TrackingDataToMdcFilter
 */
@Component
public class TrackingDataToResponseFilter extends FilterAsInterceptorAdapter {

    private final String[] excludedPaths;
    private final TrackingData trackingData;

    @Autowired
    TrackingDataToResponseFilter(
            final TrackingData trackingData,
            @Value("${logging.exclude:/private/health}") final String[] excludedPaths) {
        this.excludedPaths = excludedPaths.clone();
        this.trackingData = trackingData;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response) {
        if (!this.excludeFromLog(request.getRequestURI(), excludedPaths)) {
            response.addHeader(TrackingHeaderNames.TRACK_ID.getName(), trackingData.getTrackId());
            response.addHeader(TrackingHeaderNames.FLOW_ID.getName(), trackingData.getFlowId());
            response.addHeader(TrackingHeaderNames.REQUEST_ID.getName(), trackingData.getRequestId());
        }
        return true;
    }

}
