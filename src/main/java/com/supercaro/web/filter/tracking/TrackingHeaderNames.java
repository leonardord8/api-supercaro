package com.supercaro.web.filter.tracking;

public enum TrackingHeaderNames {

    TRACK_ID("X-Track-Id"),
    FLOW_ID("X-Flow-Id"),
    REQUEST_ID("X-Request-Id"),
    CLIENT_APPLICATION_NAME("X-Application-Name");

    private String name;

    TrackingHeaderNames(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
