package com.supercaro.web.filter.tracking;

import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.supercaro.web.filter.FilterAsInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class that generates tracking headers for every incoming request.
 *
 * @see TrackingDataToResponseFilter
 */
@Component
public class TrackingDataToMdcFilter extends FilterAsInterceptorAdapter {

    private final TrackingData trackingData;

    @Autowired
    public TrackingDataToMdcFilter(final TrackingData trackingData) {
        this.trackingData = trackingData;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response) {

        MDC.put(TrackingHeaderNames.TRACK_ID.getName(), trackingData.getTrackId());
        MDC.put(TrackingHeaderNames.FLOW_ID.getName(), trackingData.getFlowId());
        MDC.put(TrackingHeaderNames.REQUEST_ID.getName(), trackingData.getRequestId());
        MDC.put(TrackingHeaderNames.CLIENT_APPLICATION_NAME.getName(), trackingData.getClientApplicationName());

        return true;
    }
}
