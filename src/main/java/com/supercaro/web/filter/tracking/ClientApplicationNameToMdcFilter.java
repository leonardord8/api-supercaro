package com.supercaro.web.filter.tracking;

import org.apache.log4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.supercaro.web.filter.FilterAsInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class that intercepts requests and can extract information about
 * the client who is making the request. Add custom logic here if
 * you need to extract and evaluate specific details from the request.
 */
@Component
public class ClientApplicationNameToMdcFilter extends FilterAsInterceptorAdapter {

    private static final String UNKNOWN_CLIENT = "UNKNOWN";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response) {

        String requestApplicationName = request.getHeader(
                TrackingHeaderNames.CLIENT_APPLICATION_NAME.getName()
        );

        if (StringUtils.isEmpty(requestApplicationName)) {
            requestApplicationName = UNKNOWN_CLIENT;
        }

        MDC.put(TrackingHeaderNames.CLIENT_APPLICATION_NAME.getName(), requestApplicationName);

        return true;
    }
}
