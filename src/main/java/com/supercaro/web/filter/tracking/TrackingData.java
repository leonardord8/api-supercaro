package com.supercaro.web.filter.tracking;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * This class responsibility is to generate and store tracking
 * fields required for logging, namely "RequestId", "TrackId" and
 * "FlowId". Its scope is request-based, so every request can be
 * traced back to its beginning.
 *
 * @see WebApplicationContext
 */
@Component
@Scope(
        value = WebApplicationContext.SCOPE_REQUEST,
        proxyMode = ScopedProxyMode.TARGET_CLASS
)
public class TrackingData {

    private final String requestId = UUID.randomUUID().toString();
    private String trackId;
    private String flowId;
    private String clientApplicationName;

    @Autowired
    public TrackingData(HttpServletRequest httpServletRequest) {
        this.trackId = StringUtils.defaultIfBlank(httpServletRequest.getHeader(TrackingHeaderNames.TRACK_ID.getName()), this.requestId);
        this.flowId = StringUtils.defaultIfBlank(httpServletRequest.getHeader(TrackingHeaderNames.FLOW_ID.getName()), this.requestId);
        setClientApplicationName(httpServletRequest.getHeader(TrackingHeaderNames.CLIENT_APPLICATION_NAME.getName()));
    }

    public String getRequestId() {
        return this.requestId;
    }

    public String getTrackId() {
        return this.trackId;
    }

    public String getFlowId() {
        return this.flowId;
    }

    public String getClientApplicationName() {
        return this.clientApplicationName;
    }

    private void setClientApplicationName(String clientApplicationName) {

        if (clientApplicationName == null || clientApplicationName.isEmpty()) {
            this.clientApplicationName = "UNKNOWN";
        } else {
            this.clientApplicationName = clientApplicationName;
        }

    }

    @Override
    public String toString() {
        return "TrackingData{" +
                "requestId='" + this.requestId + '\'' +
                ", trackId='" + this.trackId + '\'' +
                ", flowId='" + this.flowId + '\'' +
                ", clientApplicationName='" + this.clientApplicationName + '\'' +
                '}';
    }
}