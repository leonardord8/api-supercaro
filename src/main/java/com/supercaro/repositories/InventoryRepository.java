package com.supercaro.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.supercaro.entities.InventoryEntity;
/**
 * Inventory Repository
 * @author supercaro
 */
@Repository
public interface InventoryRepository extends CrudRepository<InventoryEntity, Long> {

	@Override
	<S extends InventoryEntity> S save(S inventory);
	
	InventoryEntity findById(Long id);
}