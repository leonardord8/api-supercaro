package com.supercaro.repositories;

import java.util.List;
import java.util.Set;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.supercaro.entities.CategoryEntity;

/**
 * Category Repository
 * @author supercaro
 */
@Repository
public interface CategoryRepository extends CrudRepository<CategoryEntity, Long> {

	CategoryEntity findById(Long id);
	Set<CategoryEntity> findByIdIn(List<Long> categoriesId);
}