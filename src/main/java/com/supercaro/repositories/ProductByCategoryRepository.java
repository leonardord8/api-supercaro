package com.supercaro.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.supercaro.entities.ProductByCategoryEntity;

/**
 * ProductByCategory Repository
 * @author supercaro
 */
@Repository
public interface ProductByCategoryRepository extends CrudRepository<ProductByCategoryEntity, Long> {

	@Override
	<S extends ProductByCategoryEntity> S save(S product);
	
	ProductByCategoryEntity findById(Long id);
}