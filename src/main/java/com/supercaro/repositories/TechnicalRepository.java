package com.supercaro.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.supercaro.entities.TechnicalEntity;

/**
 * Technical Repository
 * @author supercaro
 */
@Repository
public interface TechnicalRepository extends CrudRepository<TechnicalEntity, Long> {
	
	TechnicalEntity findByDni(Long dni);
}