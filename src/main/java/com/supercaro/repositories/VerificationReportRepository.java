package com.supercaro.repositories;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.supercaro.entities.VerificationReportEntity;

/**
 * VerificationReportRepository Repository
 * @author supercaro
 */
@Repository
public interface VerificationReportRepository extends CrudRepository<VerificationReportEntity, Long> {

	@Override
	<S extends VerificationReportEntity> Iterable<S> save(Iterable<S> verificationReport);
	
	Set<VerificationReportEntity> findByLotIdOrderByDateTimeDesc(Long lotId);
}