package com.supercaro.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.supercaro.entities.LotEntity;

/**
 * Lot Repository
 * @author supercaro
 */
@Repository
public interface LotRepository extends CrudRepository<LotEntity, Long> {

	@Override
	<S extends LotEntity> S save(S product);
	
	LotEntity findById(Long id);
	
	LotEntity findByLotNumber(Long lotNumber);
}