package com.supercaro.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.supercaro.entities.RegulatoryEntity;

/**
 * Regulatory Repository
 * @author supercaro
 */
@Repository
public interface RegulatoryRepository extends CrudRepository<RegulatoryEntity, Long> {

	RegulatoryEntity findById(Long id);
}