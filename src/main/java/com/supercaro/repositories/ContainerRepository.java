package com.supercaro.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.supercaro.entities.ContainerEntity;

/**
 * Container Repository
 * @author supercaro
 */
@Repository
public interface ContainerRepository extends CrudRepository<ContainerEntity, Long> {

	ContainerEntity findById(Long id);
}