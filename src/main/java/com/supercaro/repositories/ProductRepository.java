package com.supercaro.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.supercaro.entities.ProductEntity;

/**
 * Product Repository
 * @author supercaro
 */
@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, Long> {

	@Override
	<S extends ProductEntity> S save(S product);
	
	ProductEntity findById(Long id);
	
	ProductEntity findByRnpaCode(Long rnpaCode);
	
	ProductEntity findByLotId(Long lotId);
}