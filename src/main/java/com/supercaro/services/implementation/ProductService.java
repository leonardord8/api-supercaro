package com.supercaro.services.implementation;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;
import com.supercaro.domain.dto.ProductDTO;
import com.supercaro.entities.CategoryEntity;
import com.supercaro.entities.ContainerEntity;
import com.supercaro.entities.LotEntity;
import com.supercaro.entities.ProductEntity;
import com.supercaro.entities.RegulatoryEntity;
import com.supercaro.repositories.CategoryRepository;
import com.supercaro.repositories.ContainerRepository;
import com.supercaro.repositories.LotRepository;
import com.supercaro.repositories.ProductRepository;
import com.supercaro.repositories.RegulatoryRepository;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.rest.domain.ServiceStatus;
import com.supercaro.services.interfaces.ProductServiceInterface;
import com.supercaro.util.DateHelper;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import static java.util.Objects.isNull;

/**
 * Product Service
 * @author supercaro
 */
@Service
public class ProductService implements ProductServiceInterface {

	private final ProductRepository productRepository;
	private final RegulatoryRepository regulatoryRepository;
	private final CategoryRepository categoryRepository;
	private final ContainerRepository containerRepository;
	private final LotRepository lotRepository;

	/**
	 * Constructor
	 * @param productRepository
	 */
	public ProductService(ProductRepository productRepository, 
			RegulatoryRepository regulatoryRepository,
			CategoryRepository categoryRepository,
			ContainerRepository containerRepository,
			LotRepository lotRepository) {
		this.productRepository = productRepository;
		this.regulatoryRepository = regulatoryRepository;
		this.categoryRepository = categoryRepository;
		this.containerRepository = containerRepository;
		this.lotRepository = lotRepository;
	}

	/**
	 * Method that get a product by Id
	 * @return JSONResponse<ProductEntity>
	 */
	@Transactional
	public JSONResponse<ProductEntity> getProductById(Long id) {
		ProductEntity product = null;
		try {
			product = productRepository.findById(id);
			if (isNull(product)) {
				throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
			}
		} catch (DataAccessException ex) {
			// TODO: Logger
			throw ex;
		}
		return new JSONResponse<ProductEntity>(
				new ServiceStatus(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase()), product);
	}

	/**
	 * Method that save new product
	 * @return JSONResponse<ProductDTO>
	 */
	@Transactional
	public JSONResponse<ProductDTO> saveProductAndOtherShits(ProductDTO productDTO) {
		try {
			ContainerEntity container = containerRepository.findById(productDTO.getProductContainerId());
			
			Set<CategoryEntity> setCategories = new HashSet<>();
			setCategories = categoryRepository.findByIdIn(productDTO.getProductCategories());
			
			RegulatoryEntity regulatory = regulatoryRepository.findById(productDTO.getLotRegulatoryId());
		
			LotEntity lot = LotEntity.builder()
									.lotNumber(productDTO.getLotNumber())
									.regulatoryId(regulatory.getId())
									.arrivalDate(new Date())
									.quantity(productDTO.getLotQuantity())
									.amount(productDTO.getLotAmount())
									.brand(productDTO.getLotBrand())
									.elaborationDate(DateHelper.stringToDate(productDTO.getElaborationDate()))
									.build();

			lotRepository.save(lot);
			
			ProductEntity product = ProductEntity.builder()
					.rnpaCode(productDTO.getProductRnpaCode())
					.lotId(lot.getId())
					.containerId(container.getId())
					.name(productDTO.getProductName())
					.description(productDTO.getProductDescription())
					.status(productDTO.getProductStatus())
					.addedPercentage(productDTO.getProductAddedPercentage())
					.categories(setCategories)
					.build();
			
			product = productRepository.save(product);
		} catch (DataAccessException ex) {
			// TODO: Logger
			throw ex;
		}
		return new JSONResponse<ProductDTO>(
				new ServiceStatus(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase()), productDTO);
	}
}
