package com.supercaro.services.implementation;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;
import com.supercaro.domain.dto.InventoryDTO;
import com.supercaro.domain.dto.VerificationReportDTO;
import com.supercaro.entities.InventoryEntity;
import com.supercaro.entities.ProductEntity;
import com.supercaro.entities.TechnicalEntity;
import com.supercaro.entities.VerificationReportEntity;
import com.supercaro.repositories.InventoryRepository;
import com.supercaro.repositories.ProductRepository;
import com.supercaro.repositories.TechnicalRepository;
import com.supercaro.repositories.VerificationReportRepository;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.rest.domain.ServiceStatus;
import com.supercaro.services.interfaces.InventoryServiceInterface;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import static java.util.Objects.isNull;

/**
 * Inventory Service
 * @author supercaro
 */
@Service
public class InventoryService implements InventoryServiceInterface {

	private final InventoryRepository inventoryRepository;
	private final TechnicalRepository technicalRepository;
	private final ProductRepository productRepository;
	private final VerificationReportRepository verificationReportRepository;

	/**
	 * Constructor
	 * @param inventoryRepository
	 * @param productRepository
	 * @param technicalRepository
	 * @param verificationReportRepository
	 */
	public InventoryService(
			InventoryRepository inventoryRepository,
			TechnicalRepository technicalRepository,
			ProductRepository productRepository,
			VerificationReportRepository verificationReportRepository) {
		this.inventoryRepository = inventoryRepository;
		this.technicalRepository = technicalRepository;
		this.productRepository = productRepository;
		this.verificationReportRepository = verificationReportRepository;
	}

	/**
	 * Method that get an inventory by Id
	 * @return JSONResponse<InventoryDTO>
	 */
	@Transactional
	public JSONResponse<InventoryEntity> getInventoryById(Long id) {
		InventoryEntity inventory = null;
		try {
			inventory = inventoryRepository.findById(id);
			if (isNull(inventory)) {
				throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
			}
		} catch (DataAccessException ex) {
			// TODO: Logger
			throw ex;
		}
		return new JSONResponse<InventoryEntity>(
				new ServiceStatus(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase()), inventory);
	}

	/**
	 * Method that save new inventory
	 * @return JSONResponse<InventoryDTO>
	 */
	@Transactional
	public JSONResponse<InventoryDTO> saveInventoryAndOtherShits(InventoryDTO inventoryDTO) {
		try {
			TechnicalEntity technical = technicalRepository.findByDni(inventoryDTO.getTechnicalDni());
			
			ProductEntity product = productRepository.findByRnpaCode(inventoryDTO.getProductRnpaCode());
			
			InventoryEntity inventory = InventoryEntity.builder()
											.inventoryNumber(inventoryDTO.getInventoryNumber())
											.weekDay(inventoryDTO.getInventoryWeekDay())
											.weekNumber(inventoryDTO.getInventoryWeekNumber())
											.year(inventoryDTO.getInventoryYear())
											.technicalId(technical.getId())
											.observations(inventoryDTO.getInventoryObservations())
											.build();
			
			Set<VerificationReportEntity> setVerificationReport = new HashSet<>();
			
			inventory = inventoryRepository.save(inventory);
			
			for (VerificationReportDTO verificationReportDTO : inventoryDTO.getVerififactionReportList()) {
				VerificationReportEntity vr = VerificationReportEntity.builder()
												.verificationByMrId(verificationReportDTO.getVerificationByMrId())
												.status(verificationReportDTO.getVerificationStatus())
												.dateTime(new Date())
												.lotId(product.getLot().getId())
												.inventoryId(inventory.getId())
												.build();
				setVerificationReport.add(vr);
			}
			
			verificationReportRepository.save(setVerificationReport);
			
			product.setStatus(inventoryDTO.getProductStatus());
			productRepository.save(product);
			
		} catch (DataAccessException ex) {
			// TODO: Logger
			throw ex;
		}
		return new JSONResponse<InventoryDTO>(
				new ServiceStatus(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase()), inventoryDTO);
	}
}
