package com.supercaro.services.implementation;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;
import com.supercaro.entities.RegulatoryEntity;
import com.supercaro.repositories.RegulatoryRepository;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.rest.domain.ServiceStatus;
import com.supercaro.services.interfaces.RegulatoryServiceInterface;

import static java.util.Objects.isNull;
import java.util.List;

/**
 * Regulatory Service
 * @author supercaro
 */
@Service
public class RegulatoryService implements RegulatoryServiceInterface {

	private final RegulatoryRepository regulatoryRepository;

	/**
	 * Constructor
	 * @param regulatoryRepository
	 */
	public RegulatoryService(RegulatoryRepository regulatoryRepository) {
		this.regulatoryRepository = regulatoryRepository;
	}

	/**
	 * Method that get all regulatories
	 * @return JSONResponse<List<RegulatoryEntity>>
	 */
	@Transactional
	public JSONResponse<List<RegulatoryEntity>> findAllRegulatories() {
		List<RegulatoryEntity> lRegulatories = null;
		try {
			lRegulatories = (List<RegulatoryEntity>) regulatoryRepository.findAll();
			if (isNull(lRegulatories) || lRegulatories.isEmpty()) {
				throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
			}
		} catch (DataAccessException ex) {
			// TODO: Logger
			throw ex;
		}
		return new JSONResponse<List<RegulatoryEntity>>(
				new ServiceStatus(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase()), lRegulatories);
	}

}
