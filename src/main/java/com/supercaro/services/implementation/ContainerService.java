package com.supercaro.services.implementation;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;
import com.supercaro.entities.ContainerEntity;
import com.supercaro.repositories.ContainerRepository;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.rest.domain.ServiceStatus;
import com.supercaro.services.interfaces.ContainerServiceInterface;
import static java.util.Objects.isNull;
import java.util.List;

/**
 * Container Service
 * @author supercaro
 */
@Service
public class ContainerService implements ContainerServiceInterface {

	private final ContainerRepository containerRepository;

	/**
	 * Constructor
	 * @param containerRepository
	 */
	public ContainerService(ContainerRepository containerRepository) {
		this.containerRepository = containerRepository;
	}

	/**
	 * Method that get all containers
	 * @return JSONResponse<List<ContainerEntity>>
	 */
	@Transactional
	public JSONResponse<List<ContainerEntity>> findAllContainers() {
		List<ContainerEntity> lContainers = null;
		try {
			lContainers = (List<ContainerEntity>) containerRepository.findAll();
			if (isNull(lContainers) || lContainers.isEmpty()) {
				throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
			}
		} catch (DataAccessException ex) {
			// TODO: Logger
			throw ex;
		}
		return new JSONResponse<List<ContainerEntity>>(
				new ServiceStatus(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase()), lContainers);
	}

}
