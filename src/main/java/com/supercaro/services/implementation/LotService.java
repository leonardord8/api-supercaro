package com.supercaro.services.implementation;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;
import com.supercaro.domain.dto.LotComplianceDTO;
import com.supercaro.domain.dto.VerificationReportForComplianceDTO;
import com.supercaro.entities.LotEntity;
import com.supercaro.entities.ProductEntity;
import com.supercaro.entities.VerificationReportEntity;
import com.supercaro.repositories.LotRepository;
import com.supercaro.repositories.ProductRepository;
import com.supercaro.repositories.VerificationReportRepository;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.rest.domain.ServiceStatus;
import com.supercaro.services.interfaces.LotServiceInterface;
import java.util.Set;
import static java.util.Objects.isNull;
import java.util.HashSet;

/**
 * Lot Service
 * @author supercaro
 */
@Service
public class LotService implements LotServiceInterface {

	private final LotRepository lotRepository;
	private final ProductRepository productRepository;
	private final VerificationReportRepository verificationReportRepository;

	/**
	 * Constructor
	 * @param lotRepository
	 * @param productRepository
	 * @param verificationReportRepository
	 */
	public LotService(LotRepository lotRepository,
			ProductRepository productRepository,
			VerificationReportRepository verificationReportRepository) {
		this.lotRepository = lotRepository;
		this.productRepository = productRepository;
		this.verificationReportRepository = verificationReportRepository;
	}
	
	/**
	 * Method that get an Lot Compliance DTO by lot Number
	 * @return JSONResponse<LotComplianceDTO>
	 */
	@Transactional
	public JSONResponse<LotComplianceDTO> getLotComplianceByLotNumber(Long lotNumber) {
		LotComplianceDTO lotCompliance = null;
		LotEntity lot = null;
		Set<VerificationReportEntity> verifications = null;
		Set<VerificationReportForComplianceDTO> verificationDTOSet = new HashSet<VerificationReportForComplianceDTO>();
		ProductEntity product = null;
		try {
			lot = lotRepository.findByLotNumber(lotNumber);
			product = productRepository.findByLotId(lot.getId());
			verifications = verificationReportRepository.findByLotIdOrderByDateTimeDesc(lot.getId());
			if (isNull(lot) || isNull(product) || isNull(verifications)) {
				throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
			}
			String observations = verifications.stream().findFirst().get().getInventory().getObservations();
			
			for (VerificationReportEntity verification : verifications) {
				VerificationReportForComplianceDTO verificationDTO = VerificationReportForComplianceDTO.builder()
											.regulatoryCode(lot.getRegulatory().getCode())
											.verificationName(verification.getVerficationByRegulatory().getVerification().getName())
											.verificationDescription(verification.getVerficationByRegulatory().getVerification().getDescription())
											.verificationDate(verification.getDateTime().toString())
											.verificationStatus(verification.getStatus())
											.build();
				verificationDTOSet.add(verificationDTO);
			}
			
			lotCompliance = LotComplianceDTO.builder()
								.lotNumber(product.getLot().getLotNumber())
								.verifications(verificationDTOSet)
								.actualStatus(product.getStatus())
								.lastObservations(observations)
								.build();
										
		} catch (DataAccessException ex) {
			// TODO: Logger
			throw ex;
		}
		return new JSONResponse<LotComplianceDTO>(
				new ServiceStatus(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase()), lotCompliance);
	}
}
