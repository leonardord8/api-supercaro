package com.supercaro.services.implementation;

import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;
import com.supercaro.entities.CategoryEntity;
import com.supercaro.repositories.CategoryRepository;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.rest.domain.ServiceStatus;
import com.supercaro.services.interfaces.CategoryServiceInterface;
import static java.util.Objects.isNull;
import java.util.List;

/**
 * Category Service
 * @author supercaro
 */
@Service
public class CategoryService implements CategoryServiceInterface {

	private final CategoryRepository categoryRepository;

	/**
	 * Constructor
	 * @param categoryRepository
	 */
	public CategoryService(CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}

	/**
	 * Method that get all categories
	 * @return JSONResponse<List<CategoryEntity>>
	 */
	@Transactional
	public JSONResponse<List<CategoryEntity>> findAllCategories() {
		List<CategoryEntity> lCategories = null;
		try {
			lCategories = (List<CategoryEntity>) categoryRepository.findAll();
			if (isNull(lCategories) || lCategories.isEmpty()) {
				throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
			}
		} catch (DataAccessException ex) {
			// TODO: Logger
			throw ex;
		}
		return new JSONResponse<List<CategoryEntity>>(
				new ServiceStatus(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase()), lCategories);
	}

}
