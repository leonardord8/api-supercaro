package com.supercaro.services.interfaces;

import java.util.List;
import com.supercaro.entities.RegulatoryEntity;
import com.supercaro.rest.domain.JSONResponse;

/**
 * 	Regulatory Service Interface
 * @author supercaro
 */
public interface RegulatoryServiceInterface {
	
	JSONResponse<List<RegulatoryEntity>> findAllRegulatories();
}