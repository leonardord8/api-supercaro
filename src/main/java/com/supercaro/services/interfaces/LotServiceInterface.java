package com.supercaro.services.interfaces;

import com.supercaro.domain.dto.LotComplianceDTO;
import com.supercaro.rest.domain.JSONResponse;

/**
 * Inventory Service Interface
 * @author supercaro
 */
public interface LotServiceInterface {
	
	JSONResponse<LotComplianceDTO> getLotComplianceByLotNumber(Long lotNumber);
}