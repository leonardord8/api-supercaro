package com.supercaro.services.interfaces;

import java.util.List;
import com.supercaro.entities.ContainerEntity;
import com.supercaro.rest.domain.JSONResponse;

/**
 * Container Service Interface
 * @author supercaro
 */
public interface ContainerServiceInterface {
	
	JSONResponse<List<ContainerEntity>> findAllContainers();
}