package com.supercaro.services.interfaces;

import com.supercaro.domain.dto.ProductDTO;
import com.supercaro.entities.ProductEntity;
import com.supercaro.rest.domain.JSONResponse;

/**
 * Product Service Interface
 * @author supercaro
 */
public interface ProductServiceInterface {
	
	JSONResponse<ProductEntity> getProductById(Long id);
	JSONResponse<ProductDTO> saveProductAndOtherShits(ProductDTO product);
}