package com.supercaro.services.interfaces;

import java.util.List;

import com.supercaro.entities.CategoryEntity;
import com.supercaro.rest.domain.JSONResponse;

/**
 * Category Service Interface
 * @author supercaro
 */
public interface CategoryServiceInterface {
	
	JSONResponse<List<CategoryEntity>> findAllCategories();
}