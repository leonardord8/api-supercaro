package com.supercaro.services.interfaces;

import com.supercaro.domain.dto.InventoryDTO;
import com.supercaro.entities.InventoryEntity;
import com.supercaro.rest.domain.JSONResponse;

/**
 * Inventory Service Interface
 * @author supercaro
 */
public interface InventoryServiceInterface {
	
	JSONResponse<InventoryEntity> getInventoryById(Long id);
	JSONResponse<InventoryDTO> saveInventoryAndOtherShits(InventoryDTO inventoryDTO);
}