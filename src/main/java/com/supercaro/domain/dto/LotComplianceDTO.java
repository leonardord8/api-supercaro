package com.supercaro.domain.dto;

import java.util.Set;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder()
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class LotComplianceDTO {
	
	
	@Getter
	@Setter
	@JsonProperty(value = "lotNumber")
	private Long lotNumber;
	
	@Getter
	@Setter
	@JsonProperty(value = "verifications")
	private Set<VerificationReportForComplianceDTO> verifications;
	
	@Getter
	@Setter
	@JsonProperty(value = "actualStatus")
	private String actualStatus;
	
	@Getter
	@Setter
	@JsonProperty(value = "lastObservations")
	private String lastObservations;
	
}
