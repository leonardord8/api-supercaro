package com.supercaro.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder()
@JsonIgnoreProperties(ignoreUnknown = true)
public class VerificationReportForComplianceDTO {
	
	@Getter
	@Setter
	@JsonProperty(value = "regulatoryCode")
	private Long regulatoryCode;
	
	@Getter
	@Setter
	@JsonProperty(value = "verificationDate")
	private String verificationDate;
	
	@Getter
	@Setter
	@JsonProperty(value = "verificationName")
	private String verificationName;
	
	@Getter
	@Setter
	@JsonProperty(value = "verificationDescription")
	private String verificationDescription;
	
	@Getter
	@Setter
	@JsonProperty(value = "verificationStatus")
	private Boolean verificationStatus;
	
}
