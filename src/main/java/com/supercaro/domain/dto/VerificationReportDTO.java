package com.supercaro.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder()
@JsonIgnoreProperties(ignoreUnknown = true)
public class VerificationReportDTO {
	
	@Getter
	@Setter
	@JsonProperty(value = "verificationByMrId")
	private Long verificationByMrId;
	
	@Getter
	@Setter
	@JsonProperty(value = "verificationStatus")
	private Boolean verificationStatus;
}
