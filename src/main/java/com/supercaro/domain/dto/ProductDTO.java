package com.supercaro.domain.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTO {

	@Getter
	@Setter
	@JsonProperty(value = "containerId")
	private Long productContainerId;
	
	@Getter
	@Setter
	@JsonProperty(value = "categories")
	private List<Long> productCategories;
	
	@Getter
	@Setter
	@JsonProperty(value = "lotNumber")
	private Long lotNumber;
	
	@Getter
	@Setter
	@JsonProperty(value = "lotRegulatoryId")
	private Long lotRegulatoryId;
	
	@Getter
	@Setter
	@JsonProperty(value = "lotQuantity")
	private Long lotQuantity;
	
	@Getter
	@Setter
	@JsonProperty(value = "lotAmount")
	private Double lotAmount;
	
	@Getter
	@Setter
	@JsonProperty(value = "lotBrand")
	private String lotBrand;
	
	@Getter
	@Setter
	@JsonProperty(value = "lotElaborationDate")
    private String elaborationDate;
	
	@Getter
	@Setter
	@JsonProperty(value = "productRnpaCode")
    private Long productRnpaCode;

	@Getter
	@Setter
	@JsonProperty(value = "productName")
	@NotNull
	@Pattern(regexp = "^(\\w|\\W){1,45}$", message = "Invalid param.")
	private String productName;

	
	@Getter
	@Setter
	@JsonProperty(value = "productDescription")
	@Pattern(regexp = "^(\\w|\\W){1,255}$", message = "Invalid param.")
	private String productDescription;

	
	@Getter
	@Setter
	@JsonProperty(value = "productStatus")
	@NotNull
	@Pattern(regexp = "^(D|P|V|R)$", message = "Invalid param.")
	private String productStatus;
	
	@Getter
	@Setter
	@JsonProperty(value = "productAddedPercentage")
	@NotNull
	private Double productAddedPercentage;
}
