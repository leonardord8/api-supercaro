package com.supercaro.domain.dto;

import java.util.List;

import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryDTO {

	@Getter
	@Setter
	@JsonProperty(value = "technicalDni")
	private Long technicalDni;
	
	@Getter
	@Setter
	@JsonProperty(value = "productRnpaCode")
	private Long productRnpaCode;
	
	@Getter
	@Setter
	@JsonProperty(value = "inventoryNumber")
	private Long inventoryNumber;
	
	@Getter
	@Setter
	@JsonProperty(value = "inventoryWeekDay")
	private String inventoryWeekDay;
	
	@Getter
	@Setter
	@JsonProperty(value = "inventoryWeekNumber")
	private Integer inventoryWeekNumber;
	
	@Getter
	@Setter
	@JsonProperty(value = "inventoryYear")
	private Integer inventoryYear;
	
	@Getter
	@Setter
	@JsonProperty(value = "inventoryObservations")
	private String inventoryObservations;
	
	@Getter
	@Setter
	@JsonProperty(value = "verifications")
	private List<VerificationReportDTO> verififactionReportList;
	
	@Getter
	@Setter
	@JsonProperty(value = "productStatus")
	@Pattern(regexp = "^(D|P|V|R)$", message = "Invalid param.")
	private String productStatus;
}
