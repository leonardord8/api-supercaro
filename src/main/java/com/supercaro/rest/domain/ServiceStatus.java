package com.supercaro.rest.domain;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class that represent the Service Status
 * @author supercaro
 */
public class ServiceStatus implements Serializable {

	private static final long serialVersionUID = -2231918671674427297L;
	
	@JsonProperty("code")
    private int code;
	@JsonProperty("message")
    private String message;

	@JsonCreator
    public ServiceStatus(@JsonProperty("code") int code, @JsonProperty("message") String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ServiceStatus that = (ServiceStatus) o;

        if (code != that.code) {
            return false;
        }
        return message.equals(that.message);

    }

    @Override
    public int hashCode() {
        int result = code;
        result = 31 * result + message.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ServiceStatus{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}