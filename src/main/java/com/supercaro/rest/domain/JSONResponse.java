package com.supercaro.rest.domain;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class that represent a JSON Response
 * @author supercaro
 */
public class JSONResponse<T> implements Serializable {

	private static final long serialVersionUID = 4625327040449595591L;
	
	@JsonProperty("status")
	private ServiceStatus status;
	@JsonProperty("data")
    private T data;
	
	@JsonCreator
    public JSONResponse() {
        //Constructor
    }

	@JsonCreator
    public JSONResponse(@JsonProperty("status") ServiceStatus status, @JsonProperty("data") T data) {
        this.status = status;
        this.data = data;
    }

    public ServiceStatus getStatus() {
        return status;
    }

    public void setStatus(ServiceStatus status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JSONResponse<?> that = (JSONResponse<?>) o;

        if (!status.equals(that.status)) {
            return false;
        }
        return data.equals(that.data);

    }

    @Override
    public int hashCode() {
        int result = status.hashCode();
        result = 31 * result + data.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "JSONResponse{" +
                "status=" + status +
                ", data=" + data +
                '}';
    }
}