package com.supercaro.rest.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.supercaro.domain.dto.InventoryDTO;
import com.supercaro.entities.InventoryEntity;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.services.interfaces.InventoryServiceInterface;

/**
 * Inventory Controller Class
 * @author supercaro
 */
@RestController
public class InventoryController {

	private final InventoryServiceInterface inventoryService;
	
	private class InventoryResponseDTO extends JSONResponse<InventoryDTO>{
		private static final long serialVersionUID = 7353089592069487880L;
	}
	
	private class InventoryResponseEntity extends JSONResponse<InventoryEntity>{
		private static final long serialVersionUID = -7953757264035304868L;
	}
	
	/**
	 * Constructor
	 * @param inventoryService
	 */
	public InventoryController(InventoryServiceInterface inventoryService) {
		this.inventoryService = inventoryService;
	}

	/**
	 * Method that return a Inventory by Id
	 * @param id
	 * @return ResponseEntity<JSONResponse<InventoryEntity>>
	 */
	@RequestMapping(
			value = "/inventory/{id}", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ApiOperation(
			value = "Return a Inventory", 
			notes = "Given the id", 
			response = JSONResponse.class
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok", response = InventoryResponseEntity.class),
			@ApiResponse(code = 400, message = "Bad Request."),
			@ApiResponse(code = 404, message = "Not found."),
			@ApiResponse(code = 500, message = "Internal server error") 
			}
	)
	public ResponseEntity<JSONResponse<InventoryEntity>> getInventoryById(@PathVariable(value = "id") Long id) {
		JSONResponse<InventoryEntity> jsonResponse = inventoryService.getInventoryById(id);
		return new ResponseEntity<>(jsonResponse, HttpStatus.valueOf(jsonResponse.getStatus().getCode()));
	}

	/**
	 * Method that save a new Product
	 * @param ProductDTO
	 * @return ResponseEntity<JSONResponse<InventoryDTO>>
	 */
	@RequestMapping(
			value = "/inventory", 
			method = RequestMethod.POST, 
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ApiOperation(
			value = "Return the saved Iventory", 
			notes = "Given a Inventory to save", 
			response = JSONResponse.class
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok", response = InventoryResponseDTO.class),
			@ApiResponse(code = 400, message = "Bad Request."),
			@ApiResponse(code = 500, message = "Internal server error") 
			}
	)
	public ResponseEntity<JSONResponse<InventoryDTO>> saveProduct(@Valid @RequestBody InventoryDTO inventoryDTO) {
		JSONResponse<InventoryDTO> jsonResponse = inventoryService.saveInventoryAndOtherShits(inventoryDTO);
		return new ResponseEntity<>(jsonResponse, HttpStatus.valueOf(jsonResponse.getStatus().getCode()));
	}
}