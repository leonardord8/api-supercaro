package com.supercaro.rest.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.supercaro.entities.ContainerEntity;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.services.interfaces.ContainerServiceInterface;

/**
 * Container Controller Class
 * @author supercaro
 */
@RestController
public class ContainerController {

	private final ContainerServiceInterface containerService;

	private class ListContainers extends JSONResponse<List<ContainerEntity>>{
		private static final long serialVersionUID = 6454742051486102897L;
	}
	
	/**
	 * Constructor
	 * @param containerService
	 */
	public ContainerController(ContainerServiceInterface containerService) {
		this.containerService = containerService;
	}

	/**
	 * Method that return all containers
	 * @param id
	 * @return ResponseEntity<JSONResponse<List<ContainerEntity>>>
	 */
	@RequestMapping(
			value = "/container", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ApiOperation(
			value = "Return all Containers", 
			notes = "", 
			response = JSONResponse.class
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok", response = ListContainers.class),
			@ApiResponse(code = 400, message = "Bad Request."),
			@ApiResponse(code = 404, message = "Not found."),
			@ApiResponse(code = 500, message = "Internal server error") 
			}
	)
	public ResponseEntity<JSONResponse<List<ContainerEntity>>> getAllContainers() {
		JSONResponse<List<ContainerEntity>> jsonResponse = containerService.findAllContainers();
		return new ResponseEntity<>(jsonResponse, HttpStatus.valueOf(jsonResponse.getStatus().getCode()));
	}
	
}