package com.supercaro.rest.controller;

import javax.validation.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException;

import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.rest.domain.ServiceStatus;

/**
 * Class that handle exceptions globally
 * @author supercaro
 */
@ControllerAdvice
public class RestResponseExceptionHandler {
 
	/**
     * Method responsible of handle MethodArgumentNotValidException
     * @param ex
     * @return ResponseEntity<JSONResponse<String>>
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    protected ResponseEntity<JSONResponse<String>> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
    	JSONResponse<String> jResponse = new JSONResponse<String>();
        jResponse.setStatus(new ServiceStatus(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase()));
        jResponse.setData("Invalid request. Exception=" + ex.getClass().getName());
        return new ResponseEntity<JSONResponse<String>>(jResponse, HttpStatus.valueOf(jResponse.getStatus().getCode()));
    }
    
    /**
     * Method responsible of handle ConstraintViolationException
     * @param ex
     * @return ResponseEntity<JSONResponse<String>>
     */
    @ExceptionHandler(value = ConstraintViolationException.class)
    protected ResponseEntity<JSONResponse<String>> handleConstraintViolationException(ConstraintViolationException ex) {
    	JSONResponse<String> jResponse = new JSONResponse<String>();
        jResponse.setStatus(new ServiceStatus(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase()));
        jResponse.setData("Invalid request. Exception=" + ex.getClass().getName());
        return new ResponseEntity<JSONResponse<String>>(jResponse, HttpStatus.valueOf(jResponse.getStatus().getCode()));
    }
    
    /**
     * Method responsible of handle HttpServerErrorException
     * @param ex
     * @return ResponseEntity<JSONResponse<String>>
     */
    @ExceptionHandler(value = HttpServerErrorException.class)
    protected ResponseEntity<JSONResponse<String>> handleHttpServerErrorException(HttpServerErrorException ex) {
    	JSONResponse<String> jResponse = new JSONResponse<String>();
        switch (ex.getStatusCode()){
            case UNAUTHORIZED:
                jResponse.setStatus(new ServiceStatus(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase()));
                break;
            case NOT_FOUND:
            	jResponse.setStatus(new ServiceStatus(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND.getReasonPhrase()));
                break;
            case SERVICE_UNAVAILABLE:
            	jResponse.setStatus(new ServiceStatus(HttpStatus.SERVICE_UNAVAILABLE.value(), HttpStatus.SERVICE_UNAVAILABLE.getReasonPhrase()));
                break;
            case INTERNAL_SERVER_ERROR:
            	jResponse.setStatus(new ServiceStatus(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()));
                break;
            default:
            	jResponse.setStatus(new ServiceStatus(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()));
        }
        return new ResponseEntity<>(jResponse, HttpStatus.valueOf(jResponse.getStatus().getCode()));
    }
    
    /**
     * Method responsible of handle DataAccessException
     * @param ex
     * @return ResponseEntity<JSONResponse<String>>
     */
    @ExceptionHandler(value = DataAccessException.class)
    public ResponseEntity<JSONResponse<String>> handleDataAccessException(DataAccessException ex) {
        JSONResponse<String> jResponse = new JSONResponse<String>();
        jResponse.setStatus(new ServiceStatus(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()));
        return new ResponseEntity<>(jResponse, HttpStatus.valueOf(jResponse.getStatus().getCode()));
    }
}