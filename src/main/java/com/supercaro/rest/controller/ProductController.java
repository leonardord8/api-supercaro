package com.supercaro.rest.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.supercaro.domain.dto.ProductDTO;
import com.supercaro.entities.ProductEntity;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.services.interfaces.ProductServiceInterface;

/**
 * Product Controller Class
 * @author supercaro
 */
@RestController
public class ProductController {

	private final ProductServiceInterface productService;

	private class ProductResponseDTO extends JSONResponse<ProductDTO>{
		private static final long serialVersionUID = 4226034329322885940L;
	}
	
	private class ProductResponseEntity extends JSONResponse<ProductEntity>{
		private static final long serialVersionUID = 1808641178989179303L;
	}
	
	/**
	 * Constructor
	 * @param productService
	 */
	public ProductController(ProductServiceInterface productService) {
		this.productService = productService;
	}

	/**
	 * Method that return a Product By Id
	 * @param id
	 * @return ResponseEntity<JSONResponse<ProductDTO>>
	 */
	@RequestMapping(
			value = "/product/{id}", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ApiOperation(
			value = "Return a Product", 
			notes = "Given the id", 
			response = JSONResponse.class
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok", response = ProductResponseEntity.class),
			@ApiResponse(code = 400, message = "Bad Request."),
			@ApiResponse(code = 404, message = "Not found."),
			@ApiResponse(code = 500, message = "Internal server error") 
			}
	)
	public ResponseEntity<JSONResponse<ProductEntity>> getProductById(@PathVariable(value = "id") Long id) {
		JSONResponse<ProductEntity> jsonResponse = productService.getProductById(id);
		return new ResponseEntity<>(jsonResponse, HttpStatus.valueOf(jsonResponse.getStatus().getCode()));
	}

	/**
	 * Method that save a new Product
	 * @param ProductDTO
	 * @return ResponseEntity<JSONResponse<ProductDTO>>
	 */
	@RequestMapping(
			value = "/product", 
			method = RequestMethod.POST, 
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ApiOperation(
			value = "Return the saved Product", 
			notes = "Given a Product to save", 
			response = JSONResponse.class
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok", response = ProductResponseDTO.class),
			@ApiResponse(code = 400, message = "Bad Request."),
			@ApiResponse(code = 500, message = "Internal server error") 
			}
	)
	public ResponseEntity<JSONResponse<ProductDTO>> saveProduct(@Valid @RequestBody ProductDTO productDTO) {
		JSONResponse<ProductDTO> jsonResponse = productService.saveProductAndOtherShits(productDTO);
		return new ResponseEntity<>(jsonResponse, HttpStatus.valueOf(jsonResponse.getStatus().getCode()));
	}
	
}