package com.supercaro.rest.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.supercaro.entities.RegulatoryEntity;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.services.interfaces.RegulatoryServiceInterface;

/**
 * Regulatory Controller Class
 * @author supercaro
 */
@RestController
public class RegulatoryController {

	private final RegulatoryServiceInterface regulatoryService;

	private class ListRegulatories extends JSONResponse<List<RegulatoryEntity>>{
		private static final long serialVersionUID = -6370768612343690947L;
	}
	
	/**
	 * Constructor
	 * @param regulatoryService
	 */
	public RegulatoryController(RegulatoryServiceInterface regulatoryService) {
		this.regulatoryService = regulatoryService;
	}

	/**
	 * Method that return a Product By Id
	 * @param id
	 * @return ResponseEntity<JSONResponse<List<RegulatoryEntity>>>
	 */
	@RequestMapping(
			value = "/regulatory", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ApiOperation(
			value = "Return all Regulatories", 
			notes = "", 
			response = JSONResponse.class
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok", response = ListRegulatories.class),
			@ApiResponse(code = 400, message = "Bad Request."),
			@ApiResponse(code = 404, message = "Not found."),
			@ApiResponse(code = 500, message = "Internal server error") 
			}
	)
	public ResponseEntity<JSONResponse<List<RegulatoryEntity>>> getAllRegulatories() {
		JSONResponse<List<RegulatoryEntity>> jsonResponse = regulatoryService.findAllRegulatories();
		return new ResponseEntity<>(jsonResponse, HttpStatus.valueOf(jsonResponse.getStatus().getCode()));
	}
	
	
	
}