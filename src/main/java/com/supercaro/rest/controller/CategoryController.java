package com.supercaro.rest.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.supercaro.entities.CategoryEntity;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.services.interfaces.CategoryServiceInterface;

/**
 * Category Controller Class
 * @author supercaro
 */
@RestController
public class CategoryController {

	private final CategoryServiceInterface categoryService;

	
	private class ListCategories extends JSONResponse<List<CategoryEntity>>{
		private static final long serialVersionUID = 1822829345209699072L;
	}
	
	/**
	 * Constructor
	 * @param categoryService
	 */
	public CategoryController(CategoryServiceInterface categoryService) {
		this.categoryService = categoryService;
	}

	/**
	 * Method that return all categories
	 * @param id
	 * @return ResponseEntity<JSONResponse<List<CategoryEntity>>>
	 */
	@RequestMapping(
			value = "/category", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ApiOperation(
			value = "Return all Categories", 
			notes = "", 
			response = JSONResponse.class
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok", response = ListCategories.class),
			@ApiResponse(code = 400, message = "Bad Request."),
			@ApiResponse(code = 404, message = "Not found."),
			@ApiResponse(code = 500, message = "Internal server error") 
			}
	)
	public ResponseEntity<JSONResponse<List<CategoryEntity>>> getAllCategories() {
		JSONResponse<List<CategoryEntity>> jsonResponse = categoryService.findAllCategories();
		return new ResponseEntity<>(jsonResponse, HttpStatus.valueOf(jsonResponse.getStatus().getCode()));
	}
	
}