package com.supercaro.rest.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.supercaro.domain.dto.LotComplianceDTO;
import com.supercaro.rest.domain.JSONResponse;
import com.supercaro.services.interfaces.LotServiceInterface;

/**
 * Lot Controller Class
 * @author supercaro
 */
@RestController
public class LotController {

	private final LotServiceInterface lotService;
	
	private class LotCompliance extends JSONResponse<LotComplianceDTO>{
		private static final long serialVersionUID = 2328797285879841583L;
	}
	
	/**
	 * Constructor
	 * @param lotService
	 */
	public LotController(LotServiceInterface lotService) {
		this.lotService = lotService;
	}
	
	/**
	 * Method that return a Lot Compliance By lot Number
	 * @param lotNumber
	 * @return ResponseEntity<JSONResponse<LotComplianceDTO>>
	 */
	@RequestMapping(
			value = "/lot/compliance/{lotNumber}", 
			method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ApiOperation(
			value = "Return a Lot Compliance", 
			notes = "Given the lot number", 
			response = JSONResponse.class
	)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Ok", response = LotCompliance.class),
			@ApiResponse(code = 400, message = "Bad Request."),
			@ApiResponse(code = 404, message = "Not found."),
			@ApiResponse(code = 500, message = "Internal server error") 
			}
	)
	public ResponseEntity<JSONResponse<LotComplianceDTO>> getLotComplianceByNumber(@PathVariable(value = "lotNumber") Long lotNumber) {
		JSONResponse<LotComplianceDTO> jsonResponse = lotService.getLotComplianceByLotNumber(lotNumber);
		return new ResponseEntity<>(jsonResponse, HttpStatus.valueOf(jsonResponse.getStatus().getCode()));
	}
	
}