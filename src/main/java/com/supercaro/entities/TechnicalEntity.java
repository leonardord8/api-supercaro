package com.supercaro.entities;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Entity class that represent a Patient
 * @author supercaro
 */
@Entity(name = "Technical")
@Table(name = "tecnico", schema = "supercaro")
public class TechnicalEntity implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5146741636152688727L;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	private Long id;
	
	@Column(name = "dni", unique = true, nullable=false)
	@Getter
	@Setter
	private Long dni;
	
	@Column(name = "nombre", nullable=false)
	@Getter
	@Setter
	private String name;
	
	@Column(name = "apellido", nullable=false)
	@Getter
	@Setter
	private String lastname;
	
	@Column(name = "email", nullable=false)
	@Getter
	@Setter
	private String email;
	
}