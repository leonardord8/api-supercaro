package com.supercaro.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Entity class that represent a Patient
 * @author supercaro
 */
@Builder()
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Lot")
@Table(name = "lote", schema = "supercaro")
public class LotEntity implements Serializable {

	private static final long serialVersionUID = 4237633758011265345L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Long id;
	
	@Column(name = "id_mr", nullable=false)
	@Getter
	@Setter
	private Long regulatoryId;
	
	@Column(name = "numero_lote", unique = true, nullable=false)
	@Getter
	@Setter
	private Long lotNumber;
	
	@Column(name = "marca", nullable=false)
	@Getter
	@Setter
	private String brand;
	
	@Column(name = "cantidad", nullable=false)
	@Getter
	@Setter
	private Long quantity;
	
	@Column(name = "precio_compra", nullable=false)
	@Getter
	@Setter
	private Double amount;
	
	@Column(name = "fecha_llegada", nullable=false)
	@Getter
	@Setter
	private Date arrivalDate;
	
	@Column(name = "fecha_elaboracion", nullable=false)
	@Getter
	@Setter
	private Date elaborationDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_mr", referencedColumnName = "id", insertable = false, updatable = false)
	@Getter
	@Setter
	private RegulatoryEntity regulatory;
	
}