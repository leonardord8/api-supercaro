package com.supercaro.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Entity class that represent a Patient
 * @author supercaro
 */
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Verification")
@Table(name = "verificacion", schema = "supercaro")
public class VerificationEntity implements Serializable {

	private static final long serialVersionUID = -2242888346331863050L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Long id;
	
	@Column(name = "nombre", nullable=false)
	@Getter
	@Setter
	private String name;
	
	@Column(name = "descripcion", nullable=false)
	@Getter
	@Setter
	private String description;
	
}