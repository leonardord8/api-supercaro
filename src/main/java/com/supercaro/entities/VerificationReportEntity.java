package com.supercaro.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Entity class that represent a Patient
 * @author supercaro
 */
@Builder()
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "VerificationReport")
@Table(name = "reporte_verificacion", schema = "supercaro")
public class VerificationReportEntity implements Serializable {

	private static final long serialVersionUID = 157428122071049862L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Long id;
	
	@Column(name = "id_lote", nullable=false)
	@Getter
	@Setter
	private Long lotId;
	
	@Column(name = "id_ficha", nullable=false)
	@Getter
	@Setter
	private Long inventoryId;
	
	@Column(name = "id_verificacion_por_mr", nullable=false)
	@Getter
	@Setter
	private Long verificationByMrId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_lote", referencedColumnName = "id", insertable = false, updatable = false)
	@Getter
	@Setter
	private LotEntity lot;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_ficha", referencedColumnName = "id", insertable = false, updatable = false)
	@Getter
	@Setter
	private InventoryEntity inventory;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_verificacion_por_mr", referencedColumnName = "id", insertable = false, updatable = false)
	@Getter
	@Setter
	private VerificationByRegulatoryEntity verficationByRegulatory;
	
	@Column(name = "fecha_hora", nullable=false)
	@Getter
	@Setter
	private Date dateTime;
	
	@Column(name = "estado", nullable=false)
	@Getter
	@Setter
	private Boolean status;
	
}