package com.supercaro.entities;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Entity class that represent a Patient
 * @author supercaro
 */
@Entity(name = "Category")
@Table(name = "categoria", schema = "supercaro")
public class CategoryEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2040907872955315300L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true)
	@Getter
	@Setter
	private Long id;
	
	@Column(name = "nombre", unique = true, nullable=false)
	@Getter
	@Setter
	private String name;
	
	@Column(name = "descripcion", nullable=false)
	@Getter
	@Setter
	private String description;
	
}