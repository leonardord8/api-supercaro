package com.supercaro.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Entity class that represent a Patient
 * @author supercaro
 */
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Regulatory")
@Table(name = "marco_regulatorio", schema = "supercaro")
public class RegulatoryEntity implements Serializable {

	private static final long serialVersionUID = -6612080887276758616L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Long id;
	
	@ManyToMany()
	@JoinTable(name = "verificacion_por_mr",
    	joinColumns = @JoinColumn(name = "id_mr"),
    	inverseJoinColumns = @JoinColumn(name = "id_verificacion"))
    @Getter
    @Setter
    private Set<VerificationEntity> verifications;
	
	@Column(name = "codigo", unique = true, nullable=false)
	@Getter
	@Setter
	private Long code;
	
	@Column(name = "duracion_max", nullable=false)
	@Getter
	@Setter
	private Integer durationMax;
	
	@Column(name = "temp_max", nullable=false)
	@Getter
	@Setter
	private Integer tempMax;
	
	@Column(name = "temp_min", nullable=false)
	@Getter
	@Setter
	private Integer tempMin;
}