package com.supercaro.entities;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Entity class that represent a Patient
 * @author supercaro
 */
@Entity(name = "Container")
@Table(name = "envase", schema = "supercaro")
public class ContainerEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7917983471724691205L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	private Long id;
	
	@Column(name = "tipo", nullable=false)
	@Getter
	@Setter
	private String type;
	
	@Column(name = "material", nullable=false)
	@Getter
	@Setter
	private String material;
	
	@Column(name = "peso", nullable=false)
	@Getter
	@Setter
	private Double weight;
	
	@Column(name = "volumen", nullable=false)
	@Getter
	@Setter
	private Double volume;
	
	@Column(name = "descripcion", nullable=false)
	@Getter
	@Setter
	private String description;
	
}