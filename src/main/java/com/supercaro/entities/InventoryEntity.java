package com.supercaro.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Entity class that represent a Patient
 * @author supercaro
 */
@Builder()
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Inventory")
@Table(name = "ficha", schema = "supercaro")
public class InventoryEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 147699509828527213L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Long id;
	
	@Column(name = "id_tecnico", nullable=false)
	@Getter
	@Setter
	private Long technicalId;
	
	@Column(name = "numero_ficha", unique = true, nullable=false)
	@Getter
	@Setter
	private Long inventoryNumber;
	
	@Column(name = "anio", nullable=false)
	@Getter
	@Setter
	private Integer year;
	
	@Column(name = "semana_numero", nullable=false)
	@Getter
	@Setter
	private Integer weekNumber;
	
	@Column(name = "semana_dia", nullable=false)
	@Getter
	@Setter
	private String weekDay;
	
	@Column(name = "observaciones")
	@Getter
	@Setter
	private String observations;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tecnico", referencedColumnName = "id", insertable = false, updatable = false)
	@Getter
	@Setter
	private TechnicalEntity technical;
	
}