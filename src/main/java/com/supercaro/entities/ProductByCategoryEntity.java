package com.supercaro.entities;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Entity class that represent a Patient By Dentist
 * @author supercaro
 */
@Builder()
@Entity(name = "ProductByCategory")
@Table(name = "producto_pertenece_categoria", schema = "supercaro")
public class ProductByCategoryEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5262722259454726696L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	private Long id;
	
	@Column(name = "id_producto", nullable=false)
	@Getter
	@Setter
	private Long productId;
	
	@Column(name = "id_categoria", nullable=false)
	@Getter
	@Setter
	private Long categoryId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_producto", referencedColumnName = "id", insertable = false, updatable = false)
	@Getter
	@Setter
	private ProductEntity product;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_categoria", referencedColumnName = "id", insertable = false, updatable = false)
	@Getter
	@Setter
	private CategoryEntity category;
}