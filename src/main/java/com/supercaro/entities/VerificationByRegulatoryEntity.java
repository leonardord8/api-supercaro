package com.supercaro.entities;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Entity class that represent a Patient By Dentist
 * @author supercaro
 */
@Entity(name = "VerificationByRegulatory")
@Table(name = "verificacion_por_mr", schema = "supercaro")
public class VerificationByRegulatoryEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5262722259454726696L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	private Long id;
	
	@Column(name = "id_mr", nullable=false)
	@Getter
	@Setter
	private Long regulatoryId;
	
	@Column(name = "id_verificacion", nullable=false)
	@Getter
	@Setter
	private Long verificationId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_mr", referencedColumnName = "id", insertable = false, updatable = false)
	@Getter
	@Setter
	private RegulatoryEntity regulatory;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_verificacion", referencedColumnName = "id", insertable = false, updatable = false)
	@Getter
	@Setter
	private VerificationEntity verification;
}