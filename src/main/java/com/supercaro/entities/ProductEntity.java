package com.supercaro.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;

/**
 * Entity class that represent a Dentist
 * @author supercaro
 */
@Builder()
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "Product")
@Table(name = "producto", schema = "supercaro")
public class ProductEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3572343709385636422L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Long id;
	
	@Column(name = "id_lote", nullable=false)
	@Getter
	@Setter
    private Long lotId;
	
	@Column(name = "id_envase", nullable=false)
	@Getter
	@Setter
    private Long containerId;
	
	@Column(name = "rnpa", unique = true, nullable=false)
	@Getter
	@Setter
    private Long rnpaCode;

	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_lote", referencedColumnName = "id", insertable = false, updatable = false)
    @Getter
    @Setter
    private LotEntity lot;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_envase", referencedColumnName = "id", insertable = false, updatable = false)
    @Getter
    @Setter
    private ContainerEntity container;
	
	@ManyToMany()
	@JoinTable(name = "producto_pertenece_categoria",
    	joinColumns = @JoinColumn(name = "id_producto"),
    	inverseJoinColumns = @JoinColumn(name = "id_categoria"))
    @Getter
    @Setter
    private Set<CategoryEntity> categories;
	
	@Column(name = "nombre", nullable=false)
	@Getter
	@Setter
	private String name;

	@Column(name = "descripcion", nullable=false)
	@Getter
	@Setter
	private String description;

	@Column(name = "estado", nullable=false)
	@Getter
	@Setter
	private String status;
	
	@Column(name = "porcentaje_agregado", nullable=false)
	@Getter
	@Setter
	private Double addedPercentage;

}