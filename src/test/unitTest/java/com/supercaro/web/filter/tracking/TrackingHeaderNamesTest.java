package com.supercaro.web.filter.tracking;

import org.junit.Test;

import com.supercaro.web.filter.tracking.TrackingHeaderNames;

import static com.supercaro.web.filter.tracking.TrackingHeaderNames.*;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class TrackingHeaderNamesTest {

    @Test
    public void shouldHaveCorrectTrackIdHeaderName() {
        assertThat(TRACK_ID.getName(), is("X-Track-Id"));
    }

    @Test
    public void shouldHaveCorrectFlowIdHeaderName() {
        assertThat(FLOW_ID.getName(), is("X-Flow-Id"));
    }

    @Test
    public void shouldHaveCorrectRequestIdHeaderName() {
        assertThat(REQUEST_ID.getName(), is("X-Request-Id"));
    }

    @Test
    public void shouldHaveCorrectValues() {
        assertThat(asList(TrackingHeaderNames.values()), hasItems(TRACK_ID, FLOW_ID, REQUEST_ID));
    }
}