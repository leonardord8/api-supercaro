package com.supercaro.web.filter.tracking;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.supercaro.web.filter.tracking.TrackingData;
import com.supercaro.web.filter.tracking.TrackingDataToResponseFilter;
import com.supercaro.web.filter.tracking.TrackingHeaderNames;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrackingDataToResponseFilterTest {

    private static final String SOME_TRACK_ID = "fake track id";
    private static final String SOME_FLOW_ID = "fake flow id";
    private static final String SOME_APP_NAME = "fake app name";
    private static final String WS_API_MESSAGE = "/ws/api/corporate-auth/v1/message";
    private static final String WS_API_MESSAGE_HEALTH = "/ws/api/corporate-auth/v1/private/health";

    private TrackingDataToResponseFilter trackingDataToResponseFilter;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Before
    public void setUp() {
        stubServletResponseWithHeaders();
        trackingDataToResponseFilter =
                new TrackingDataToResponseFilter(
                        new TrackingData(httpServletRequest),
                        new String[] {WS_API_MESSAGE_HEALTH});
    }

    @Test
    public void shouldGetHeadersFromRequest() {
        when(httpServletRequest.getRequestURI()).thenReturn(WS_API_MESSAGE);
        boolean result = trackingDataToResponseFilter.preHandle(httpServletRequest, httpServletResponse);
        assertThat(result, is(true));
    }

    @Test
    public void shouldSetHeadersInResponse() throws Exception {
        when(httpServletRequest.getRequestURI()).thenReturn(WS_API_MESSAGE);
        boolean result = trackingDataToResponseFilter.preHandle(httpServletRequest, httpServletResponse);
        verify(httpServletResponse).addHeader(eq(TrackingHeaderNames.TRACK_ID.getName()), eq(SOME_TRACK_ID));
        verify(httpServletResponse).addHeader(eq(TrackingHeaderNames.FLOW_ID.getName()), eq(SOME_FLOW_ID));
        verify(httpServletResponse).addHeader(eq(TrackingHeaderNames.REQUEST_ID.getName()), anyString());
        assertThat(result, is(true));
    }

    @Test
    public void shouldNotSetHeadersInResponseWhenHealthCheck() throws Exception {
        when(httpServletRequest.getRequestURI()).thenReturn(WS_API_MESSAGE_HEALTH);
        boolean result = trackingDataToResponseFilter.preHandle(httpServletRequest, httpServletResponse);
        verify(httpServletResponse, never()).addHeader(eq(TrackingHeaderNames.TRACK_ID.getName()), eq(SOME_TRACK_ID));
        verify(httpServletResponse, never()).addHeader(eq(TrackingHeaderNames.FLOW_ID.getName()), eq(SOME_FLOW_ID));
        verify(httpServletResponse, never()).addHeader(eq(TrackingHeaderNames.REQUEST_ID.getName()), anyString());
        assertThat(result, is(true));
    }

    private void stubServletResponseWithHeaders() {
        when(httpServletRequest.getHeader(TrackingHeaderNames.TRACK_ID.getName())).thenReturn(SOME_TRACK_ID);
        when(httpServletRequest.getHeader(TrackingHeaderNames.FLOW_ID.getName())).thenReturn(SOME_FLOW_ID);
        when(httpServletRequest.getHeader(TrackingHeaderNames.CLIENT_APPLICATION_NAME.getName())).thenReturn(SOME_APP_NAME);
    }

}
