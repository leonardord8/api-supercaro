package com.supercaro.web.filter.tracking;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.supercaro.web.filter.tracking.TrackingData;
import com.supercaro.web.filter.tracking.TrackingHeaderNames;

import javax.servlet.http.HttpServletRequest;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrackingDataTest {

    private static final String SOME_FLOW_ID = "some flow id";
    private static final String EMPTY_STRING = "";
    private static final String SOME_TRACK_ID = "some track id";
    private static final String SOME_CLIENT_APPLICATION_NAME = "some client application name";

    @Mock
    private HttpServletRequest httpServletRequest;

    @Test
    public void shouldSetsRandomTrackIdWhenIsNotProvided() throws Exception {

        when(httpServletRequest.getHeader(anyString())).thenReturn(null);
        assertThat("Track Id is not null o empty when setting null",
                new TrackingData(httpServletRequest).getTrackId(), not(isEmptyOrNullString()));

        when(httpServletRequest.getHeader(anyString())).thenReturn(EMPTY_STRING);
        assertThat("Track Id is not null o empty when setting empty",
                new TrackingData(httpServletRequest).getTrackId(), not(isEmptyOrNullString()));
    }

    @Test
    public void shouldSetsClientApplicationNameWhenIsProvided() throws Exception {
        when(httpServletRequest.getHeader(TrackingHeaderNames.CLIENT_APPLICATION_NAME.getName()))
                .thenReturn(SOME_CLIENT_APPLICATION_NAME);
        assertThat("should Sets ClientApplicationName When Is Provided",
                new TrackingData(httpServletRequest).getClientApplicationName(), is(SOME_CLIENT_APPLICATION_NAME));
    }

    @Test
    public void shouldSetsTrackIdWhenIsProvided() throws Exception {
        when(httpServletRequest.getHeader(TrackingHeaderNames.TRACK_ID.getName()))
                .thenReturn(SOME_TRACK_ID);
        assertThat("should Sets TrackId When Is Provided",
                new TrackingData(httpServletRequest).getTrackId(), is(SOME_TRACK_ID));
    }

    @Test
    public void shouldSetsRandomFlowIdWhenIsNotProvided() throws Exception {
        when(httpServletRequest.getHeader(anyString())).thenReturn(null);
        assertThat("Flow Id is not null o empty when setting null",
                new TrackingData(httpServletRequest).getFlowId(), not(isEmptyOrNullString()));


        when(httpServletRequest.getHeader(anyString())).thenReturn("");
        assertThat("Flow Id is not null o empty when setting empty",
                new TrackingData(httpServletRequest).getFlowId(), not(isEmptyOrNullString()));
    }

    @Test
    public void shouldSetsFlowIdWhenIsProvided() throws Exception {
        when(httpServletRequest.getHeader(TrackingHeaderNames.FLOW_ID.getName()))
                .thenReturn(SOME_FLOW_ID);
        assertThat("should Sets FlowId When Is Provided",
                new TrackingData(httpServletRequest).getFlowId(), is(SOME_FLOW_ID));
    }

    @Test
    public void shouldGeneratesRandomRequestId() throws Exception {
        assertThat("should Generates Random Request Id",
                new TrackingData(httpServletRequest).getRequestId(),
                not(isEmptyOrNullString()));
    }

    @Test
    public void shouldBeEqualsToRequestIdWhenNotInformedTrackIdAndFlowIdAndClientApplicationName() {
        final TrackingData trackingData = new TrackingData(httpServletRequest);
        String requestId = trackingData.getRequestId();
        String clientApplicationName = "UNKNOWN";

        assertThat("should be equals trackId when not informed trackId",
                trackingData.getTrackId(),
                is(requestId));
        assertThat("should be equals flowId when not informed flowId",
                trackingData.getFlowId(),
                is(requestId));
        assertThat("should be equals UNKNOWN when not informed client application name",
                trackingData.getClientApplicationName(),
                is(clientApplicationName));
    }

    @Test
    public void shouldBeEqualsToUNKNOWNWhenNotClientApplicationName() {

        String clientApplicationName = "UNKNOWN";

        assertThat("should be equals UNKNOWN when not informed client application name",
                new TrackingData(httpServletRequest).getClientApplicationName(),
                is(clientApplicationName));
    }

    @Test
    public void shouldReturnInfoOnToString() {
        final TrackingData trackingData = new TrackingData(httpServletRequest);
        assertTrue(trackingData.toString().contains("requestId=") &&
                trackingData.toString().contains("trackId=") &&
                trackingData.toString().contains("flowId=") &&
                trackingData.toString().contains("clientApplicationName"));
    }

}