package com.supercaro.web.filter.tracking;

import org.apache.log4j.MDC;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.supercaro.web.filter.tracking.ClientApplicationNameToMdcFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClientApplicationNameToMdcFilterTest {

    private static final String APP_NAME = "AppTest";
    private static final String APP_NAME_HEADER = "X-Application-Name";
    private static final String UNKNOWN_CLIENT = "UNKNOWN";

    private ClientApplicationNameToMdcFilter clientApplicationNameToMdcFilter;

    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private HttpServletResponse httpServletResponse;

    @Before
    public void setup() throws Exception {
        clientApplicationNameToMdcFilter = new ClientApplicationNameToMdcFilter();
    }

    @Test
    public void shouldReturnTrueIfContainsAppNameHeader() throws IOException, ServletException {
        when(httpServletRequest.getHeader(APP_NAME_HEADER)).thenReturn(APP_NAME);
        boolean response = clientApplicationNameToMdcFilter.preHandle(httpServletRequest, httpServletResponse);
        assertThat(response, is(true));
    }

    @Test
    public void shouldContainMDCHeaderWithClientFieldAndName() throws Exception {
        when(httpServletRequest.getHeader(APP_NAME_HEADER)).thenReturn(APP_NAME);
        clientApplicationNameToMdcFilter.preHandle(httpServletRequest, httpServletResponse);
        assertThat(MDC.get(APP_NAME_HEADER), is(APP_NAME));
    }

    @Test
    public void shouldContainMDCHeaderWithClientUnknownWhenThereIsAnEmptyHeader() throws Exception {
        when(httpServletRequest.getHeader(APP_NAME_HEADER)).thenReturn("");
        clientApplicationNameToMdcFilter.preHandle(httpServletRequest, httpServletResponse);
        assertThat(MDC.get(APP_NAME_HEADER), is(UNKNOWN_CLIENT));
    }

    @Test
    public void shouldContainMDCHeaderWithClientUnknownWhenThereIsNoHeader() throws Exception {
        clientApplicationNameToMdcFilter.preHandle(httpServletRequest, httpServletResponse);
        assertThat(MDC.get(APP_NAME_HEADER), is(UNKNOWN_CLIENT));
    }
}
