package com.supercaro.web.filter.tracking;

import org.apache.log4j.MDC;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.supercaro.web.filter.tracking.TrackingData;
import com.supercaro.web.filter.tracking.TrackingDataToMdcFilter;
import com.supercaro.web.filter.tracking.TrackingHeaderNames;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class TrackingDataToMdcFilterTest {

    private TrackingDataToMdcFilter trackingDataToMdcFilter;
    private HttpServletRequest request;
    private HttpServletResponse response;

    @Before
    public void setup() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        trackingDataToMdcFilter = new TrackingDataToMdcFilter(
                new TrackingData(request)
        );
    }

    @Test
    public void shouldNeverReturnNullUsingTrackingData() {
        boolean result = trackingDataToMdcFilter.preHandle(request, response);
        assertThat(MDC.get(TrackingHeaderNames.TRACK_ID.getName()), is(notNullValue()));
        assertThat(MDC.get(TrackingHeaderNames.FLOW_ID.getName()), is(notNullValue()));
        assertThat(MDC.get(TrackingHeaderNames.REQUEST_ID.getName()), is(notNullValue()));
        assertThat(result, is(true));
    }

}
