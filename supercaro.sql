CREATE DATABASE  IF NOT EXISTS `supercaro` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `supercaro`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: supercaro
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.36-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_35t4wyxqrevf09uwx9e9p6o75` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Productos de la vaca','Lacteo'),(2,'Como la manzana','Frutas');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `envase`
--

DROP TABLE IF EXISTS `envase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `envase` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `material` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `volumen` double NOT NULL,
  `peso` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `envase`
--

LOCK TABLES `envase` WRITE;
/*!40000 ALTER TABLE `envase` DISABLE KEYS */;
INSERT INTO `envase` VALUES (1,'Descripcion Botella','Vidrio','Botella',1,1),(2,'Descripcion Cajon','Madera','Cajon',2,2);
/*!40000 ALTER TABLE `envase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ficha`
--

DROP TABLE IF EXISTS `ficha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ficha` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero_ficha` bigint(20) NOT NULL,
  `observaciones` varchar(255) DEFAULT NULL,
  `id_tecnico` bigint(20) NOT NULL,
  `semana_dia` varchar(255) NOT NULL,
  `semana_numero` int(11) NOT NULL,
  `anio` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5ijtl08x3lr0k7rtfbgulbgb` (`numero_ficha`),
  KEY `FKfo1hqau6tw2mlflfpjp9nvcl9` (`id_tecnico`),
  CONSTRAINT `FKfo1hqau6tw2mlflfpjp9nvcl9` FOREIGN KEY (`id_tecnico`) REFERENCES `tecnico` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ficha`
--

LOCK TABLES `ficha` WRITE;
/*!40000 ALTER TABLE `ficha` DISABLE KEYS */;
INSERT INTO `ficha` VALUES (1,1,'Primera Obs',1,'Lu',12,2018),(2,2,'Segunda Obs',1,'Lu',12,2018);
/*!40000 ALTER TABLE `ficha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lote`
--

DROP TABLE IF EXISTS `lote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `precio_compra` double NOT NULL,
  `fecha_llegada` datetime NOT NULL,
  `marca` varchar(255) NOT NULL,
  `fecha_elaboracion` datetime NOT NULL,
  `numero_lote` bigint(20) NOT NULL,
  `cantidad` bigint(20) NOT NULL,
  `id_mr` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_68ki158d54u397lpnnk887ild` (`numero_lote`),
  KEY `FK8gr5ckbsnn56eyye2e7f3i2u9` (`id_mr`),
  CONSTRAINT `FK8gr5ckbsnn56eyye2e7f3i2u9` FOREIGN KEY (`id_mr`) REFERENCES `marco_regulatorio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lote`
--

LOCK TABLES `lote` WRITE;
/*!40000 ALTER TABLE `lote` DISABLE KEYS */;
INSERT INTO `lote` VALUES (1,200,'2018-11-30 19:29:04','Brand','2018-11-30 00:00:00',1,100,1);
/*!40000 ALTER TABLE `lote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marco_regulatorio`
--

DROP TABLE IF EXISTS `marco_regulatorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marco_regulatorio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `codigo` bigint(20) NOT NULL,
  `duracion_max` int(11) NOT NULL,
  `temp_max` int(11) NOT NULL,
  `temp_min` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_csd06sww579wcniets1guw8r9` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marco_regulatorio`
--

LOCK TABLES `marco_regulatorio` WRITE;
/*!40000 ALTER TABLE `marco_regulatorio` DISABLE KEYS */;
INSERT INTO `marco_regulatorio` VALUES (1,201,60,90,0),(2,195,45,35,0);
/*!40000 ALTER TABLE `marco_regulatorio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `porcentaje_agregado` double NOT NULL,
  `id_envase` bigint(20) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `id_lote` bigint(20) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `rnpa` bigint(20) NOT NULL,
  `estado` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_tr2wkf4dymoe99gr0hghhnju` (`rnpa`),
  KEY `FKroe15xuj1rne2pwclswhtl7rc` (`id_envase`),
  KEY `FKq64sud5bf1k8itb5o20wvi18i` (`id_lote`),
  CONSTRAINT `FKq64sud5bf1k8itb5o20wvi18i` FOREIGN KEY (`id_lote`) REFERENCES `lote` (`id`),
  CONSTRAINT `FKroe15xuj1rne2pwclswhtl7rc` FOREIGN KEY (`id_envase`) REFERENCES `envase` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,30,1,'Description',1,'Name',123,'V');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto_pertenece_categoria`
--

DROP TABLE IF EXISTS `producto_pertenece_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto_pertenece_categoria` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_categoria` bigint(20) NOT NULL,
  `id_producto` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7jf4gavcc3p6byxvea2sotbr5` (`id_categoria`),
  KEY `FKa2287ty2ucwsvj2lo9fy21spy` (`id_producto`),
  CONSTRAINT `FK7jf4gavcc3p6byxvea2sotbr5` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id`),
  CONSTRAINT `FKa2287ty2ucwsvj2lo9fy21spy` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto_pertenece_categoria`
--

LOCK TABLES `producto_pertenece_categoria` WRITE;
/*!40000 ALTER TABLE `producto_pertenece_categoria` DISABLE KEYS */;
INSERT INTO `producto_pertenece_categoria` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `producto_pertenece_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporte_verificacion`
--

DROP TABLE IF EXISTS `reporte_verificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporte_verificacion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha_hora` datetime NOT NULL,
  `id_ficha` bigint(20) NOT NULL,
  `id_lote` bigint(20) NOT NULL,
  `estado` bit(1) NOT NULL,
  `id_verificacion_por_mr` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKjbmti5viptj17bpwj01yywqom` (`id_ficha`),
  KEY `FKh6kela3e7idndoqe0dlg311p7` (`id_lote`),
  KEY `FKhjfvg88bigi1mlwq7x3jh4xfn` (`id_verificacion_por_mr`),
  CONSTRAINT `FKh6kela3e7idndoqe0dlg311p7` FOREIGN KEY (`id_lote`) REFERENCES `lote` (`id`),
  CONSTRAINT `FKhjfvg88bigi1mlwq7x3jh4xfn` FOREIGN KEY (`id_verificacion_por_mr`) REFERENCES `verificacion_por_mr` (`id`),
  CONSTRAINT `FKjbmti5viptj17bpwj01yywqom` FOREIGN KEY (`id_ficha`) REFERENCES `ficha` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporte_verificacion`
--

LOCK TABLES `reporte_verificacion` WRITE;
/*!40000 ALTER TABLE `reporte_verificacion` DISABLE KEYS */;
INSERT INTO `reporte_verificacion` VALUES (1,'2018-11-30 19:29:43',1,1,'',2),(2,'2018-11-30 19:29:43',1,1,'',1),(3,'2018-11-30 19:29:59',2,1,'\0',1),(4,'2018-11-30 19:29:59',2,1,'',2);
/*!40000 ALTER TABLE `reporte_verificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tecnico`
--

DROP TABLE IF EXISTS `tecnico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnico` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dni` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_b5jwp6rtd21n1394lhj69tqfi` (`dni`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnico`
--

LOCK TABLES `tecnico` WRITE;
/*!40000 ALTER TABLE `tecnico` DISABLE KEYS */;
INSERT INTO `tecnico` VALUES (1,35466654,'ianmp91@gmail.com','Paris','Ian'),(2,37325877,'leonardord8@gmail.com','Ruiz Diaz','Leonardo');
/*!40000 ALTER TABLE `tecnico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verificacion`
--

DROP TABLE IF EXISTS `verificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verificacion` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verificacion`
--

LOCK TABLES `verificacion` WRITE;
/*!40000 ALTER TABLE `verificacion` DISABLE KEYS */;
INSERT INTO `verificacion` VALUES (1,'Tiene fecha elaboracion','Fecha elaboracion'),(2,'Cumple con temp max y min','Temperatura'),(3,'Vencimiento','Vencimiento');
/*!40000 ALTER TABLE `verificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verificacion_por_mr`
--

DROP TABLE IF EXISTS `verificacion_por_mr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verificacion_por_mr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_mr` bigint(20) NOT NULL,
  `id_verificacion` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKno8peq38dnomkocn2j7kn1lxd` (`id_mr`),
  KEY `FKo6e9f5fth37d01blpsghrgtkj` (`id_verificacion`),
  CONSTRAINT `FKno8peq38dnomkocn2j7kn1lxd` FOREIGN KEY (`id_mr`) REFERENCES `marco_regulatorio` (`id`),
  CONSTRAINT `FKo6e9f5fth37d01blpsghrgtkj` FOREIGN KEY (`id_verificacion`) REFERENCES `verificacion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verificacion_por_mr`
--

LOCK TABLES `verificacion_por_mr` WRITE;
/*!40000 ALTER TABLE `verificacion_por_mr` DISABLE KEYS */;
INSERT INTO `verificacion_por_mr` VALUES (1,1,1),(2,1,3),(3,2,1),(4,2,2);
/*!40000 ALTER TABLE `verificacion_por_mr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-30 19:36:41
