# Make Command Reference

# help
*make help*

Shows this help file

# clean
*make clean*

Removes all downloaded dependencies and runs a clean build

# build 
*make build*

Downloads dependencies and builds sources.

# run 
*make run JVM_ARG="-Dspring.profiles.active=dev"*

Runs the service in console with profile dev.

# debug
*make debug JVM_ARG="-Dspring.profiles.active=dev"*

Runs the service in console with debug with profile dev.

# unit-test
*make unit-test*

Runs unitTests.	